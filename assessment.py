def first_exercise():
    '''
    Solution to the first exercise
    '''
    def filter_function(x):
        if x%15==0:
            return 'GoDataDriven'
        elif x%5==0:
            return 'Driven'
        elif x%3==0:
            return 'GoData'
        else: return x

    tmp_list = [filter_function(x) for x in range (1,101) ]
    print(tmp_list)


#------
#second exercise
#------
def calc_potential(time, firstsale, lastsale, sold, supplied):
    '''
    original function
    
    1_change: defined variable output and appending it at the end - small performance improvement
    2_change: check if f>t sale happens before first sale, should not return a negative number - hence returns none
    3_change: check if t>l sale happens after last sale, should not return a negative number - hence returns none
    4_change: redifine loop and use list comprehension for performance imporvement
    
    '''
    def calculate_function(t, f, l, c, s):
        try:
            if s > c:
                return c 
            else:
                if f>t: # check if sale happens before first sale (non sense)
                    return None
                elif t>l:# check if sale happens after last sale (non sense)
                    return None
                else:
                # rename s to r for clarity
                    r = (l - t).total_seconds() / 3600.
                    d = ((t - f).total_seconds() / 3600.) / c
                    return r / d + c 
        except:
            return None 
   
    retval = [calculate_function(t, f, l, c, s) for (t, f, l, c, s) in zip(time, firstsale, lastsale, sold, supplied)]

    return retval


# define some unit test for second function
import unittest
from datetime import datetime as dt


class TestFunction(unittest.TestCase):

    def test_happy_flow(self):
        '''
        Test happy flow:
        comment: last component of sold is a string, should cause an exception that returns None
        '''
        str_time_of_sale = ['13:00:00','12:00:00','12:00:00']
        str_time_first_sale = ['09:00:00','10:00:00','09:00:00']
        str_time_last_sale = ['17:00:00','15:00:00','11:00:00']

        time_of_sale = [dt.strptime(x,'%H:%M:%S') for x in str_time_of_sale]
        time_first_sale = [dt.strptime(x,'%H:%M:%S') for x in str_time_first_sale]
        time_last_sale = [dt.strptime(x,'%H:%M:%S') for x in str_time_last_sale]
        sold = [5,10,'a']
        supplied = [9,2,12]

        self.assertEqual(calc_potential(time_of_sale,time_first_sale,time_last_sale,sold,supplied), [5, 25.0, None])
        
    def test_sale_beforefirstsale(self):
        '''
        Test that is the sale happens before the first sale, the code returns none
        First and second case:
        in first element, sold < supplied, so return sold
        in second element, testing purpose of test case, returns None
        '''
        str_time_of_sale = ['18:00:00','09:00:00','12:00:00']
        str_time_first_sale = ['09:00:00','10:00:00','09:00:00']
        str_time_last_sale = ['17:00:00','15:00:00','13:00:00']
        time_of_sale = [dt.strptime(x,'%H:%M:%S') for x in str_time_of_sale]
        time_first_sale = [dt.strptime(x,'%H:%M:%S') for x in str_time_first_sale]
        time_last_sale = [dt.strptime(x,'%H:%M:%S') for x in str_time_last_sale]
        sold = [5,10,12]
        supplied = [9,2,10]

        self.assertEqual(calc_potential(time_of_sale,time_first_sale,time_last_sale,sold,supplied), [5, None, 16.0])
        
    def test_sale_afterlastsale(self):
        '''
        Test that is the sale happens after the last sale, the code returns none
        First and third element:
        in first element, sold < supplied, so return sold
        in third element, testing purpose of test case, returns None
        '''
        str_time_of_sale = ['18:00:00','11:00:00','12:00:00']
        str_time_first_sale = ['09:00:00','10:00:00','09:00:00']
        str_time_last_sale = ['17:00:00','15:00:00','10:00:00']
        time_of_sale = [dt.strptime(x,'%H:%M:%S') for x in str_time_of_sale]
        time_first_sale = [dt.strptime(x,'%H:%M:%S') for x in str_time_first_sale]
        time_last_sale = [dt.strptime(x,'%H:%M:%S') for x in str_time_last_sale]
        
        sold = [5,10,12]
        supplied = [9,2,10]
        
        self.assertEqual(calc_potential(time_of_sale,time_first_sale,time_last_sale,sold,supplied), [5, 50.0, None])



if __name__ == '__main__':
    first_exercise()
    unittest.main()

