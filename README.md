Solution to the assessment:

Running:
python assessment.py

returns output of first exercise, and unit tests of the second exercise

Changes to the second exercise are documented in the code, as well as in the git commit history